ActiveAdmin.register Dish do

  permit_params :place_id, :name, :price, :desc, :picture

  index do
    column :picture do |dish|
      if dish.picture.attached?
        image_tag dish.picture, size: "200"
      end
    end
    column :name
    column :price
    column :desc
    column :place_id do |dish|
      link_to dish.place.name, admin_place_path(dish.place)
    end
    actions
  end

  show do
    attributes_table do
      row :image do |dish|
        if dish.picture.attached?
          image_tag dish.picture, size: "280x250"
        end
      end
      row :name
      row :price
      row :desc
      row :place do |dish|
        dish.place
      end
    end
  end


  form do |f|
   f.semantic_errors *f.object.errors.keys
   f.inputs do
    f.input :place_id,
    :label => 'Places',
    :as => :select,
    :collection => Place.all.map{ |place| ["#{place.name}", place.id] },
    include_blank: false
    f.input :name
    f.input :price, min: 1
    f.input :desc, min: 0
    if f.object.picture.attached?
      f.input :picture,
      :as => :file
    else
      f.input :picture, :as => :file
    end

  end
  f.actions
end
end
