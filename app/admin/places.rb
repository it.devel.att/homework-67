ActiveAdmin.register Place do

  permit_params :name, :desc, :avatar

  index do
    id_column
    column :picture do |place|
      if place.avatar.attached?
        image_tag place.avatar, size: "150x100"
      end
    end
    column :name
    column :desc
    actions
  end

  show do
    attributes_table do
      row :avatar do |place|
        if place.avatar.attached?
          image_tag place.avatar, size: "150x100"
        end
      end
      row :name
      row :desc
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    inputs do
      if f.object.avatar.attached?
        f.input :avatar,
        :as => :file
      else
        f.input :avatar, :as => :file
      end
      f.input :name
      f.input :desc
    end
    f.submit
  end

end
