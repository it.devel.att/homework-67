class DishesController < ApplicationController
  before_action :authenticate_user!, only: [:make_order]
  before_action :set_dish, only: [:show, :make_order, :plus_qty_to_product, :minus_qty_to_product]

  def index
    @dishes = Dish.all
  end

  def show
  end

  def make_order
    session[:order_cart] ||= {}
    session[:order_cart]["#{@dish.place.id}"] ||= {}
    session[:order_cart]["#{@dish.place.id}"]["orders"] ||= []
    already =  session[:order_cart]["#{@dish.place.id}"]["orders"].find{ |order| order["dish_id"] == @dish.id }

    if already
      add_qty_product
    else
      session[:order_cart]["#{@dish.place.id}"]["orders"].push({"dish_id" => @dish.id, "price" => @dish.price, "qty" => 1})
    end

    redirect_back(fallback_location:root_path)
  end

  def plus_qty_to_product
    session[:order_cart]["#{@dish.place.id}"]["orders"].each do |order|
      if order["dish_id"] == @dish.id
        order["qty"] += 1
      end
    end
    redirect_back(fallback_location:root_path)
  end

  def minus_qty_to_product
    session[:order_cart]["#{@dish.place.id}"]["orders"].each do |order|
      if order["dish_id"] == @dish.id
        order["qty"] -= 1 if order["qty"] > 1
      end
    end
    redirect_back(fallback_location:root_path)
  end

  private
  def add_qty_product
    session[:order_cart]["#{@dish.place.id}"]["orders"].each do |order|
      if order["dish_id"] == @dish.id
        order["qty"] += 1
      end
    end
  end

  def set_dish
    @dish = Dish.find(params[:id])
  end
end
