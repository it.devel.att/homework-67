class PlacesController < ApplicationController
  before_action :set_place, only: [:show]

  def index
    @places = Place.all
  end

  def show
    @dishes = @place.dishes

    if session[:order_cart] && session[:order_cart]["#{@place.id}"]
      @shop_cart = []
      dish = nil
      @total_price = 0
      session[:order_cart]["#{@place.id}"]["orders"].each do |order|
        dish = Dish.find(order["dish_id"])
        price = order["price"]
        qty = order["qty"]
        item = { dish: dish, price: price, qty: qty, sum: qty.to_i * price.to_f }
        @shop_cart.push(item)
        @total_price += item[:sum]
      end
    end

  end

  private
  def set_place
    @place = Place.find(params[:id])
  end
end
