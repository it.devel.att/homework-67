# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, Dish
    can :read, Place
    if user.present?
      can :make_order, Dish
        if user.admin?
            can :manage, :all
        end
    end
  end
end
