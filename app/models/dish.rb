class Dish < ApplicationRecord
  belongs_to :place

  has_one_attached :picture

  validates :name, presence: true, length: { in: 2..50 }
  validates :desc, presence: true, length: { in: 2..250 }
  validates :price, presence: true, numericality: { only_float: true, greater_than: 1 }
  end
