class Place < ApplicationRecord
  has_many :dishes, dependent: :destroy
  has_one_attached :avatar

  validates :name, presence: true, length: { in: 2..50 }
  validates :desc, presence: true, length: { in: 2..500 }
end
