class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable

  validates :name, presence: true, length: { in: 2..20 }
  validates :phone, presence: true, length: { in: 5..20 }
  validates :adress, presence: true, length: { in: 5..100 }
end
