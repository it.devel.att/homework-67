Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  devise_for :users
  root 'places#index'
  resources :places, only: [:index, :show]
  resources :dishes, only: [] do
    member do
      post :make_order
      put :plus_qty_to_product
      put :minus_qty_to_product
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
