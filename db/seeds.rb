admin = User.create(
  name: 'admin_admin',
  password: 'awesome_admin',
  email: 'admin@admin.com',
  phone: '0777777777',
  adress: 'Myami Beach',
  admin: true
  )
User.create(
  name: 'sample_user',
  password: 'sample_user',
  email: 'sample@user.com',
  phone: '013131313',
  adress: 'Tagil'
  )

5.times do |i|
  path_file = Rails.root.join('app', 'assets', 'images', 'fixtures', 'places', "#{i+1}.jpg")
  place = Place.create(name: Faker::Company.name, desc: Faker::Lorem.paragraph_by_chars)
  place.avatar.attach(io: File.open(path_file), filename: "#{i+1}.jpg")
end

dir = 1
Place.all.each do |place|
  puts "_____________"
  puts place.name
  8.times do |i|
    path_file = Rails.root.join('app', 'assets', 'images', 'fixtures', 'dishes', "#{dir}", "#{i+1}.jpg")
    new_dish = place.dishes.create(name: Faker::Food.dish, price: rand(10..2500), desc: Faker::Food.description)
    new_dish.picture.attach(io: File.open(path_file), filename: "#{i+1}.jpg")
    puts "#{i} - #{new_dish.name}"
  end
  dir += 1
end
